#!/bin/sh

# Gonna hate openssl

TMP=$(mktemp -d)


SERIAL=$(cat serial)
NEWSERIAL=$(expr "$SERIAL" + 1)
printf "%012d" "$NEWSERIAL" > "serial"


cp openssl.cnf "$TMP/openssl.cnf"
cp serial "$TMP/serial"
cp "$1" "$TMP/cert"
cp "$2" "$TMP/key"

cat /dev/stdin > "$TMP/csr"

cd "$TMP"

mkdir newcerts
touch index.txt

exec openssl ca -batch -days 1000 -notext -config openssl.cnf -in "$TMP/csr" 2>/dev/null
