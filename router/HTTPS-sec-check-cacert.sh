#!/bin/bash
# Script for checking HTTPS Security �lab objectives
# Objective name - Creating a Certificate and Private Key

# Author - Katrin Loodus
# Modified by  Roland Kaur
#
# Date - 28.04.2016
# Version - 0.0.1

LC_ALL=C

# START
# HTTPSSECCACERT

# Set variables

START () {

	# Enable logging
	echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
	exec &> >(tee -a /var/log/labcheckslog.log)

	# If $CheckFile exists, then exit the script
	CheckFile="/tmp/cacert"

	if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

    # Exit if there are undeclared variables
    set -o nounset     

	# Get working directory
	DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

	# IP to SSH to - devops server
	IP_to_SSH=192.168.6.2

	# Time to sleep between running the check again
	Sleep=5

	# Objective uname in VirtualTA
	Uname=certreq


}

# User interaction: Set apache to listen to port 8080

HTTPSSECCACERT () {

	while true
	do

   	# Check if Private key is created into /etc/ssl/private/* and cetificate is created into /etc/ssl/certs/* 

	ssh root@$IP_to_SSH "grep 'BEGIN CERTIFICATE' /etc/ssl/certs/server.lab.crt"
#	ssh root@$IP_to_SSH "grep 'BEGIN RSA PRIVATE KEY' /etc/ssl/private/priv.key && grep 'BEGIN CERTIFICATE REQUEST' /etc/ssl/certs/certreq.csr"

   	# Run objectiveschecks.py and update VirtualTa with correct value
    	if [ $? -eq 0 ]; then

        	echo -e "\nCert Created!!! Date: `date`\n" && touch $CheckFile
        	$DIR/objectivechecks.py $Uname true || echo -e "\nFailed to run $DIR/objectiveschecks.py! Date: `date`" >&2 && exit 1
        	exit 0

    	else

        	echo -e "Cert not created! Date: `date`\n" >&2
        	sleep $Sleep

    	fi
	done

}

START

HTTPSSECCACERT

exit 0

