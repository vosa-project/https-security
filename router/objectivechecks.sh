#!/bin/bash

VTAFILE="/tmp/lab_data"

API_KEY=$(jq -r '.lab.lab_token' $VTAFILE)
LAB_ID=$(jq -r '.lab.lab_hash' $VTAFILE)
USER_KEY=$(jq -r '.user.user_key' $VTAFILE)
UNAME=$1
DONE=$2
VTA_HOST=$(jq -r '.assistant.uri' $VTAFILE)

while true; do

	curl -H "Content-Type: application/json" -X PUT -d '{"api_key":"'"$API_KEY"'", "labID":"'"$LAB_ID"'", "user":"'"$USER_KEY"'", "uname":"'"$UNAME"'", "done":'$DONE'}' $VTA_HOST/api/v2/labuser_any/

	if [ $? -ne 0 ]; then
		echo "Cannot connect to VTA"
		sleep 2
	else
		echo "Objective/Step successfully updated"
		break
	fi
done

