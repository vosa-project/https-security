#!/bin/bash
# Script for checking HTTPS Security lab objectives
# Objective name - Enabling HTTPS on yoursite

# Author - Katrin Loodus
# Modified by  Roland Kaur
#
# Date - 01.05.2016
# Version - 0.0.1

LC_ALL=C

# START
# HTTPSSECSSLENDIS

# Set variables

START () {

	# Enable logging
	echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
	exec &> >(tee -a /var/log/labcheckslog.log)

	# If $CheckFile exists, then exit the script
	CheckFile="/tmp/sslendis"

	if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

    # Exit if there are undeclared variables
    set -o nounset     

	# Get working directory
	DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

	# IP to SSH to - devops server
	IP_to_SSH=192.168.6.2

	# Time to sleep between running the check again
	Sleep=5

	# Objective uname in VirtualTA
	Uname=sslendis


}

# User interaction: enable SSL and yoursite-ssl

HTTPSSECSSLENDIS () {

	while true
	do

   	# Check if SSLv3 is disabled on Apache and TLSv1.2 is enabled on Nginx
	
	ssh root@$IP_to_SSH "echo 'x' | openssl s_client -connect $IP_to_SSH:443 -ssl3 | grep 'no peer certificate' && echo 'x' | openssl s_client -connect $IP_to_SSH:8443 -tls1_2 | grep 'BEGIN CERTIFICATE'"

   	# Run objectiveschecks.py and update VirtualTa with correct value
    	if [ $? -eq 0 ]; then

        	echo -e "\nApache SSLv3 disabled and nginx SSLv3 enabled!! Date: `date`\n" && touch $CheckFile
        	$DIR/objectivechecks.py $Uname true || echo -e "\nFailed to run $DIR/objectiveschecks.py! Date: `date`" >&2 && exit 1
        	exit 0

    	else

        	echo -e "Apache SSLv3 not disabled and nginx SSLv3 not enabled!! Date: `date`\n" >&2
        	sleep $Sleep

    	fi
	done

}

START

HTTPSSECSSLENDIS

exit 0

