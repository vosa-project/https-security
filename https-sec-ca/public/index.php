<?php

#$cacert = './intermediate.crt';
#$cakey = './intermediate.key';
$cacert = './ca.crt';
$cakey = './ca.key';

if(isset($_POST['csr'])) {
	$csr = $_POST['csr'];


	$descriptorspec = array(
		0 => array('pipe', 'r'),
		1 => array('pipe', 'w'),
		2 => array('pipe', 'w')
	);

	chdir('..');

	$process = proc_open("./sign.sh '$cacert' '$cakey'", $descriptorspec, $pipes);

	fwrite($pipes[0], $csr);
	fclose($pipes[0]);

	$cert = stream_get_contents($pipes[1]);
	fclose($pipes[1]);

	$returnValue = proc_close($process);
	if($returnValue !== 0) {
		die('Signing failed');
	}

	touch('/tmp/certificate-signed');

}

?>
<!DOCTYPE html>
<html>
<head>
	<link rel="shortcut icon" type="image/png" href="unlocked.png"/>

	<meta charset="utf-8"/>
	<title>CertificateAuthority</title>
	<link rel="stylesheet" href="bootstrap-3.3.7-dist/css/bootstrap.min.css">
	<script src="jquery-3.1.0.min.js"></script>
	<script src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
<style>

	.navbar {
		border-radius: 0;
	}
	.navbar-brand>img {
	    display: inline-block;
	    vertical-align: middle;
	}
	.logo {
		width: 70%;
		margin:auto;
		display: block; 
	}
	.container {
		margin-top:60px;
	}
	textarea {
		background-color:#f1f1f1!important; 
	}
</style>

</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
    	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="#">
      	<img src="unlocked.png" alt="logo"/> CertificateAuthority
      </a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
	    <ul class="nav navbar-nav navbar-right">
	      <li class="active"><a href="#top">Home</a></li>
	      <li><a href="#about">About</a></li>
	      <li><a href="#sign">Sign</a></li>
	      <li><a href="#root">Root certificate</a></li>
	    </ul>
	</div>
  </div>
</nav>
<div class="container">
	<div id="home" class="row lead">
		<div class="col-xs-3 visible-md visible-lg visible-xl">
			<img class="logo" src="unlocked.svg" alt="logo">
		</div>
		<div class="col-xs-9">
			<h1>Secure your connection</h1>
			<p>It is easy, free and most importantly, it is what makes your users trust you!</p>
			<p><a href="#sign">Submit your request now</a> and reap all the benefits of secure web.</p>
		</div>
	</div>
	<div id="about" class="well">
		<h2>What is a certificate?</h2>
		<p>A signed certificate is an authorized certificate that is issued by a trustworthy CA. Signed certificates can be used to create secure connections to a server via the Internet. A client (web browser) uses a signed certificate to authenticate the CA signature on the server certificate, as part of the authorizations before launching a secure connection.</p>
	</div>
	<div id="sign" class="row">
		<?php if(isset($cert)): ?>
			<div class="col-xs-12">
				<h2>Your certificate</h2>
				<pre><?php echo $cert; ?></pre>
			</div>
		<?php else: ?>
			<form method="POST">
				<h2>Certificate Signing Request</h2>
				<section>
					<textarea name="csr" cols="73" rows="13" class="form-control"></textarea>
				</section>
				<p>
					<input type="submit" value="Sign" class="btn btn-primary form-control"  />
				</p>
			</form>
		<?php endif; ?>
	</div>
	<div id="root" class="row">
		<div class="col-md-6">
			<h3>What is a root certificate?</h3>
			<p>In cryptography and computer security, a root certificate is an unsigned or a self-signed public key certificate that identifies the root certificate authority (CA). A root certificate is part of a public key infrastructure scheme. The most common commercial variety is based on the ITU-T X.509 standard, which normally includes a digital signature from a certificate authority.</p>
		</div>
		<div class="col-md-6 well">
			<h3>Download here!</h3>
			<p class="text-center">
				<a class="btn btn-lg btn-primary" href="./ca.crt">Root CA certificate</a>
				<!--<a href="./intermediate.crt">Intermediate CA certificate</a>-->
			</p>
		</div>
	</div>
</div>
</body>
</html>
