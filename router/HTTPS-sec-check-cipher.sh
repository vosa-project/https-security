#!/bin/bash
# Script for checking HTTPS Security lab objectives
# Objective name - Enabling HTTPS on yoursite

# Author - Katrin Loodus
# Modified by  Roland Kaur
#
# Date - 03.05.2016
# Version - 0.0.1

LC_ALL=C

# START
# HTTPSSECCIPHER

# Set variables

START () {

	# Enable logging
	echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
	exec &> >(tee -a /var/log/labcheckslog.log)

	# If $CheckFile exists, then exit the script
	CheckFile="/tmp/cipher"

	if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

    # Exit if there are undeclared variables
    set -o nounset     

	# Get working directory
	DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

	# IP to SSH to - devops server
	IP_to_SSH=192.168.6.2

	# Time to sleep between running the check again
	Sleep=5

	# Objective uname in VirtualTA
	Uname=cipher


}

# User interaction: disable cipher RC4 on Apache and DES on Nginx

HTTPSSECCIPHER () {

	while true
	do

   	# Check if RC4 is disabled on Apache and DES is enabled on Nginx
	
	ssh root@$IP_to_SSH "echo 'x' | openssl s_client -connect $IP_to_SSH:8443 -cipher DES | grep 'Secure Renegotiation IS NOT supported' && echo 'x' | openssl s_client -connect $IP_to_SSH:443 -cipher RC4 | grep 'Secure Renegotiation IS NOT supported'"

   	# Run objectiveschecks.py and update VirtualTa with correct value
    	if [ $? -eq 0 ]; then

        	echo -e "\nApache RC4 disabled and nginx DES disabled!! Date: `date`\n" && touch $CheckFile
        	$DIR/objectivechecks.py $Uname true || echo -e "\nFailed to run $DIR/objectiveschecks.py! Date: `date`" >&2 && exit 1
        	exit 0

    	else

        	echo -e "Apache RC4 not disabled and nginx DES not disabled!! Date: `date`\n" >&2
        	sleep $Sleep

    	fi
	done

}

START

HTTPSSECCIPHER

exit 0

