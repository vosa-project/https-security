#!/bin/bash
# Script for checking HTTPS Security lab objectives
# Objective name - Enabling HTTPS on yoursite

# Author - Katrin Loodus
# Modified by  Roland Kaur
#
# Date - 29.04.2016
# Version - 0.0.1

LC_ALL=C

# START
# HTTPSSECYOURSITE2

# Set variables

START () {

	# Enable logging
	echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
	exec &> >(tee -a /var/log/labcheckslog.log)

	# If $CheckFile exists, then exit the script
	CheckFile="/tmp/yoursite2"

	if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

    # Exit if there are undeclared variables
    set -o nounset     

	# Get working directory
	DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

	# IP to SSH to - devops server
	IP_to_SSH=192.168.6.2

	# Time to sleep between running the check again
	Sleep=5

	# Objective uname in VirtualTA
	Uname=yoursite2


}

# User interaction: enable SSL on nginx default site

HTTPSSECYOURSITE2 () {

	while true
	do

   	# Check if nginx default site is enabled on port 443
	ssh root@$IP_to_SSH "lsof -i | grep 'nginx' | grep '8443'"
   	# Run objectiveschecks.py and update VirtualTa with correct value
    	if [ $? -eq 0 ]; then

        	echo -e "\nYoursite2 has been enabled!! Date: `date`\n" && touch $CheckFile
        	$DIR/objectivechecks.py $Uname true || echo -e "\nFailed to run $DIR/objectiveschecks.py! Date: `date`" >&2 && exit 1
        	exit 0

    	else

        	echo -e "Yoursite2 has not been enabled!! Date: `date`\n" >&2
        	sleep $Sleep

    	fi
	done

}

START

HTTPSSECYOURSITE2

exit 0

